#ifndef __SERVE_H_
#define __SERVE_H_
#include "sys.h"

#define Serve1(n)  TIM_SetCompare1(TIM3,2000-(n));
#define Serve2(n)  TIM_SetCompare2(TIM3,2000-(n));
#define Serve3(n)  TIM_SetCompare3(TIM3,2000-(n));
#define Serve4(n)  TIM_SetCompare4(TIM3,2000-(n));
#define Serve5(n)  TIM_SetCompare1(TIM4,2000-(n));
#define Serve6(n)  TIM_SetCompare2(TIM4,2000-(n));

extern u8 RGB_dat[3];

void activity_ready(void);

void Read_Show_RGB_1(void);//原料区上层识别颜色
void Read_Show_RGB_2(void);//原料区下层识别颜色

void Set_Servo(void);


void Catch_0_1_0(void);//原料区0上层
void Catch_0_1_1(void);//原料区1上层
void Catch_0_1_2(void);//原料区2上层
void Catch_0_2_0(void);//原料区0下层
void Catch_0_2_1(void);//原料区1下层
void Catch_0_2_2(void);//原料区2下层

void Catch_1_0(void);//粗加工区抓取
void Catch_1_1(void);//粗加工区抓取
void Catch_1_2(void);//粗加工区抓取

void LayDown_1_0(void);//粗加工区放置,//	while(!right_8_bin);
void LayDown_1_1(void);//粗加工区放置,//	while(!front_全部);
void LayDown_1_2(void);//粗加工区放置,//	while(!right_8_bin);

void LayDown_2_1_0(void);//半加工区0上层
void LayDown_2_1_1(void);//半加工区1上层
void LayDown_2_1_2(void);//半加工区2上层
void LayDown_2_2_0(void);//半加工区0下层
void LayDown_2_2_1(void);//半加工区1下层
void LayDown_2_2_2(void);//半加工区2下层

void lay_red(void);//放置红色物料
void lay_green(void);//放置绿色物料
void lay_blue(void);//放置蓝色物料
void catch_red(void);//放置红色物料
void catch_green(void);//放置绿色物料
void catch_blue(void);//放置蓝色物料

#endif
