////////////////////////////////////////////////////////////////////////////////////	 
////本程序只供学习使用，未经作者许可，不得用于其他用途
////技术资料唯一网站：http://www.weilaishijiejiqiren.icoc.me/
////单片机交流群：439190573
////淘宝店：https://shop130205202.taobao.com
////修改日期：2019/5/5
////版本：V2.1
////版权所有，盗版必究。
////未来世界机器人系列
////////////////////////////////////////////////////////////////////////////////////
//#include "usart3.h"
//#include "delay.h"
//#include "oled.h"

////=================================================================================
//unsigned int Temp_Data[3] = { 0 };       //激光测距数据缓存区
//unsigned char out[10] = "0";      //显示缓存
//	
///*********************************************************************
// *  函数名称：void Usart3_Init
// *  函数功能：串口3初始化
// *  形    参：无
// *  输    出：无
// *  备    注：无
// ********************************************************************/
//void Usart3_Init(void)
//{
//	GPIO_InitTypeDef   GPIO_InitStructure;
//	USART_InitTypeDef  USART_InitStructure;
//	NVIC_InitTypeDef   NVIC_InitStructure;
//	
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
//	
//	GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE);
//	
//	//USART3_TX		PC.10
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;					//复用推挽输出
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	
//	//USART3_RX		PC.11
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;	
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;			//浮空输入
//	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	
//	//对串口3通信协议进行初始化设置
//	USART_InitStructure.USART_BaudRate = 115200;						//设置波特率
//	USART_InitStructure.USART_WordLength = USART_WordLength_8b;		//字长为8位数据格式
//	USART_InitStructure.USART_StopBits = USART_StopBits_1;			//1位停止位
//	USART_InitStructure.USART_Parity = USART_Parity_No;				//无奇偶效验
//	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件流控制
//	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;	//双向通信
//	USART_Init(USART3, &USART_InitStructure);
//	
//	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);					//开启接收中断	
//	USART_Cmd(USART3, ENABLE);

//	//对串口3收发中断设置
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);					//中断组选第二组
//	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;  				
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  		//先占优先级2级
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;  			//从优先级3级
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 				//IRQ通道被使能
//	NVIC_Init(&NVIC_InitStructure);
//}

//u8 USART_RX_STA[8] = { 0 };       //接收状态标记	  
//u8 Num = 0;              //接收数据的当前位置

//	
///*********************************************************************
// *  函数名称：Read_LaserDis
// *  函数功能：读取数据
// *  形    参：ID: 模块编号,*Data: 读取到的数据
// *  输    出：无
// *  备    注：无
// ********************************************************************/   
//void Read_LaserDis(unsigned char ID, unsigned int *Data)       
//{	
//	unsigned char y=0;
//	unsigned int Receive_data [3] = { 0 };       //数据缓存区

//	Num = 0; 
/////////////////////////////读取距离、环境质量、环境光强数值///////////////////////////////
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
//	USART_SendData(USART3, 0x57);																//命令起始信号
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);	

//	while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
//	USART_SendData(USART3, ID);																  //ID模块编号
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);
//	delay_ms(2);

//  while(1)
//  {
//		if(USART_RX_STA[0] != 0x75) { Num = 0; }  //判断帧头0x75,否者重新接收
//		if(Num == 8)
//		{
//			Num = 0;
//			if(USART_RX_STA[7] == 0x07)  //判断帧尾0x07,否者不赋值
//			{
//				Receive_data[0] = USART_RX_STA[1];
//				Receive_data[0] <<= 8;
//				Receive_data[0] |= USART_RX_STA[2];        
//				*Data = Receive_data[0];          //距离
//				
//				Receive_data[1] = USART_RX_STA[3];
//				Receive_data[1] <<= 8;
//				Receive_data[1] |= USART_RX_STA[4];
//				*(Data+1) = Receive_data[1];          //环境质量
//				
//				Receive_data[2] = USART_RX_STA[5];
//				Receive_data[2] <<= 8;
//				Receive_data[2] |= USART_RX_STA[6];
//				*(Data+2) = Receive_data[2];         //环境光强        
//				
//				break;
//			}        
//			break;
//		}
//    else
//    {
//      delay_ms(1);y++;
//      if(y==10) { Num = 0;break; }
//    }
//  }
/////////////////////////////读取距离、环境质量、环境光强数值///////////////////////////////
//}
///*********************************************************************
// *  函数名称：Set_LaserDis
// *  函数功能：设置功能参数
// *  形    参：ID: 模块编号,Fun: 功能项,Par: 参数,
// *  输    出：无
// *  备    注：无
// ********************************************************************/
//void Set_LaserDis(unsigned char ID, unsigned char Fun,unsigned char Par)	       
//{	
// ///////////////////////////设置功能参数///////////////////////////////	
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
//	USART_SendData(USART3, 0x4C);																//命令起始信号
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);	
//	
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
//	USART_SendData(USART3, ID);																  //ID模块编号
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);
//	
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
//	USART_SendData(USART3, Fun);																//功能项
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);	
//	
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
//	USART_SendData(USART3, Par);																//参数
//	while(USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET);

/////////////////////////////设置功能参数///////////////////////////////		
//}
///*********************************************************************
// *  函数名称：void USART3_IRQHandler
// *  函数功能：串口3中断服务函数
// *  形    参：无
// *  输    出：无
// *  备    注：无
// ********************************************************************/
//void USART3_IRQHandler(void)
//{
//	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)  //接收中断
//	{
//		USART_RX_STA[Num++] =USART_ReceiveData(USART3);	//读取接收到的数据
//	} 	
//}

///*************************************
//*函数名称：DataShow
//*函数功能：数据转换成十进制
//*参数：Data，*p
//*说明：5位
//*     Data-输入的数据变量，*p-输出的数据变量
//**************************************/
//void DataShow(unsigned int Data,unsigned char *p)
//{
//  unsigned char a[]="0123456789";
//  *p=a[Data/10000];
//  *(p+1)=a[Data%10000/1000];
//  *(p+2)=a[Data%1000/100];
//  *(p+3)=a[Data%100/10];
//  *(p+4)=a[Data%10];
//}

//void show_distance(void)
//{
//		Read_LaserDis(0x00, Temp_Data);  //读取数据
//		
//		DataShow(Temp_Data[0],out);		//数据转换成十进制
//		OLED_ShowStr(0,0, out);		//测量距离
//}


