#ifndef  __RUN_H
#define  __RUN_H
#include "sys.h"

extern u8	Speedx;
extern u8	Speedy;

void test_left(void);
void test_right(void);
void test_back(void);
void test_front(void);
void turn_90(u8 dir);//0 顺时针，1逆时针
void lock_disable(void);
void turn_90(u8 dir);//0 顺时针，1逆时针

void adjust(void);
void adjust_Y(void);
void adjust_X(void);
void adjust_X0(void);

void add_speedy_right(u8 status);

void reduce_x(u8 speed);
void reduce_y(u8 speedy);

void Speed_left(u8 Speed);
void Speed_right(u8 Speed);
void Speed_back(u8 Speed);
void Speed_front(u8 Speed);

void add_speedx(u8 status);//x轴
void add_speedy(u8 status);//y轴

void go_front(u8 status);//前进加速寻迹
void go_back(u8 status);//后退加速寻迹
void go_left(u8 status);//左向加速寻迹
void go_right(u8 status);//右向加速寻迹

void go_0_1(void);	//从原料区出发去粗加工区
void go_1_2(void);	//从粗加工区出发去半加工区
void go_2_0(void);	//从半加工区出发去原料区
void go_0_1_two(void);	//从原料区出发去粗加工区
void go_1_2_two(void);	//从粗加工区出发去半加工区

void go_pos_x(char dest,char addr,u8 status);//当到达目标位置的前一个坐标时，调节速度行驶至目标的十字
void go_pos_y(char dest,char addr,u8 status);
void go_pos_code(char dest,char addr);

void go_start(void);
void go_home(void);

void turn_90(u8 dir);//0 顺时针，1逆时针

#endif
