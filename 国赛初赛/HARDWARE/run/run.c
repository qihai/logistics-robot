#include "run.h"

u8	Speedx=14;
u8	Speedy=14;

u8	Speed=0;

u8	posflag=0;//循迹标记数量
u8	isBlack=1;//判断是否为黑


//运动函数
void test_back(void)
{
	M_run(0,1,0,1);
	M2_dir(0);
	M4_dir(0);
}

void test_front(void)
{
	M_run(0,1,0,1);
	M2_dir(1);
	M4_dir(1);
}

void test_right(void)
{
	M_run(1,0,1,0);
	M1_dir(0);
	M3_dir(0);
}

void test_left(void)
{
	M_run(1,0,1,0);
	M1_dir(1);
	M3_dir(1);
}

void turn_90(u8 dir)//0 顺时针，1逆时针
{	
	M_run(1,1,1,1);
	M1_speed(14);
	M2_speed(14);
	M3_speed(14);
	M4_speed(14);
	if(dir)
	{	
		M1_dir(1);
		M3_dir(0);
		M2_dir(0);
		M4_dir(1);

	}

	else
	{
		M1_dir(0);
		M3_dir(1);
		M2_dir(1);
		M4_dir(0);
	}
	delay_ms(750);
	lock_disable();	
}


//运动函数
void Speed_back(u8 Speed)//front
{
	M_run(0,1,0,1);
	M2_dir(0);
	M4_dir(0);
	M2_speed(Speed);
	M4_speed(Speed);
}

void Speed_front(u8 Speed)//back
{
	M_run(0,1,0,1);
	M2_dir(1);
	M4_dir(1);
	M2_speed(Speed);
	M4_speed(Speed);
}

void Speed_right(u8 Speed)//right
{
	M_run(1,0,1,0);
	M1_dir(0);
	M3_dir(0);
	M1_speed(Speed);
	M3_speed(Speed);
}

void Speed_left(u8 Speed)
{
	M_run(1,0,1,0);
	M1_dir(1);
	M3_dir(1);
	M1_speed(Speed);
	M3_speed(Speed);
}

void lock_disable(void) //先锁电机再停止电机使能
{
	M_lock();
	delay_ms(250);
	M_run(0,0,0,0);
}



//开始
void go_start(void)
{
	Speed_front(14);

//	delay_ms(800);
	while(!right_4_bin);
	delay_ms(100);
	Speed_front(10);
	while(!right_5_bin)
	{
		if(front_2_bin && !front_7_bin)
		{
	      M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		else if(front_7_bin && !front_2_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(front_3_bin && !front_6_bin)
		{
	      M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		else if(front_6_bin && !front_3_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(front_4_bin && !front_5_bin)
		{
	      M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		else if(front_5_bin && !front_4_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(front_1_bin && !front_8_bin)
		{
	      M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		else if(front_8_bin && !front_1_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
				M2_speed(Speedy);
				M4_speed(Speedy);
		}
	}
	lock_disable();
}

void go_home(void)
{
//	Speed_back(8);
//	while(!right_6_bin);
//	lock_disable();
	
	go_pos_y(2,1,0);
	adjust();
	go_pos_x(7,4,0);
	go_pos_y(1,2,0);
	Speed_back(14);
	while(!front_1_bin);
	delay_ms(180);
	lock_disable();
	Speed_right(14);
	while(!left_1_bin);
	delay_ms(220);
	lock_disable();
}

void add_speedx(u8 status)
{
	if(status==1)
	{
		if(Speedx<16)
		{
			Speedx++;
			delay_ms(4);
			M1_speed(Speedx);
			M3_speed(Speedx);
		}
	}
	else
	{
		if(Speedx<14)
		{
			Speedx++;
			delay_ms(3);
			M1_speed(Speedx);
			M3_speed(Speedx);
		}
	}	
}


void add_speedy(u8 status)
{
	if(status==1)
	{
		if(Speedy<16)
		{
			Speedy++;
			delay_ms(4);
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
	}
	else
	{
		if(Speedy<16)
		{
			Speedy++;
			delay_ms(3);
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
	}	
}


//前向加速寻迹
void go_front(u8 status)
{
	add_speedy(status);
	if(front_2_bin && !front_7_bin)
		{
	      M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		else if(front_7_bin && !front_2_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(front_3_bin && !front_6_bin)
		{
	      M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		else if(front_6_bin && !front_3_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(front_4_bin && !front_5_bin)
		{
	      M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		else if(front_5_bin && !front_4_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(front_1_bin && !front_8_bin)
		{
	      M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		else if(front_8_bin && !front_1_bin)
		{
	    	M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
				M2_speed(Speedy);
				M4_speed(Speedy);
		}
}

//后向加速寻迹
void go_back(u8 status)
{
	add_speedy(status);
	if(back_2_bin && !back_7_bin)
		{
			M2_speed(Speedy*2/3);
			M4_speed(Speedy);
		}
		 else if(back_7_bin && !back_2_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*2/3);
		}
		else if(back_3_bin && !back_6_bin)
		{
			M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		 else if(back_6_bin && !back_3_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(back_4_bin && !back_5_bin)
		{
			M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		 else if(back_5_bin && !back_4_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(back_1_bin && !back_8_bin)
		{
			M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		 else if(back_8_bin && !back_1_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
}

//左向加速寻迹
void go_left(u8 status)
{
	add_speedx(status);
	if(left_2_bin && !left_7_bin)
	{
		M1_speed(Speedx*2/3);
		M3_speed(Speedx);
	}
	 else if(left_7_bin && !left_2_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*2/3);
	}
	else if(left_3_bin && !left_6_bin)
	{
		M1_speed(Speedx*3/4);
		M3_speed(Speedx);
	}
	 else if(left_6_bin && !left_3_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*3/4);
	}
	else if(left_4_bin && !left_5_bin)
	{
		M1_speed(Speedx*4/5);
		M3_speed(Speedx);
	}
	 else if(left_5_bin && !left_4_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*4/5);
	}
	else if(left_1_bin && !left_8_bin)
	{
		M1_speed(Speedx*1/2);
		M3_speed(Speedx);
	}
	 else if(left_8_bin && !left_1_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*1/2);
	}
	else
	{
		M1_speed(Speedx);
		M3_speed(Speedx);
	}
}
//右向寻迹
void go_right(u8 status)
{
	add_speedx(status);
	if(right_2_bin && !right_7_bin)
	{
		M1_speed(Speedx*2/3);
		M3_speed(Speedx);
	}
	 else if(right_7_bin && !right_2_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*2/3);
	}
	else if(right_3_bin && !right_6_bin)
	{
		M1_speed(Speedx*3/4);
		M3_speed(Speedx);
	}
	 else if(right_6_bin && !right_3_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*3/4);
	}
	else if(right_4_bin && !right_5_bin)
	{
		M1_speed(Speedx*4/5);
		M3_speed(Speedx);
	}
	 else if(right_4_bin && !right_5_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*4/5);
	}
	else if(right_1_bin && !right_8_bin)
	{
		M1_speed(Speedx*1/2);
		M3_speed(Speedx);
	}
	 else if(right_8_bin && !right_1_bin)
	{
		M1_speed(Speedx);
		M3_speed(Speedx*1/2);
	}
	else
	{
		M1_speed(Speedx);
		M3_speed(Speedx);
	}
///////////////////////////////////////
//	if(right_3_bin && !right_6_bin)
//		 {
//	      M1_speed(Speed*5/6);
//				M3_speed(Speed);
//		 }
//		 else if(right_6_bin && !right_3_bin)
//		{
//	    	M1_speed(Speed);
//				M3_speed(Speed*5/6);
//		 }
//		else  if(right_1_bin  && !right_8_bin)
//			{
//				M1_speed(Speed*3/4);
//				M3_speed(Speed);
//		 }
//		else if(right_7_bin && !right_2_bin)
//		{
//	    	M1_speed(Speed);
//				M3_speed(Speed*3/4);
//		 }
//		else  if(right_2_bin && !right_7_bin)
//			{
//				M1_speed(Speed*1/2);
//				M3_speed(Speed);
//		 }
//		else if(right_8_bin && !right_1_bin)
//		{
//	    	M1_speed(Speed);
//				M3_speed(Speed*1/2);
//		}
//		else 
//		{
//				M1_speed(Speed);
//				M3_speed(Speed);
//		}
}
//=================================================================================
/***********************************X轴方向循迹************************************/
//=================================================================================
//通过坐标行驶 dest:目标 addr：所在点
//车 0 1 2 3 4 5 6 7 8
void go_pos_x(char dest,char addr,u8 status)
{
		u8 res=0;
	if(dest>addr)
	{
		Speedx =0;
		res=dest-addr;//走
		posflag=0;//
		isBlack=1;
		Speed_right(Speedx);	
		delay_ms(150);
		while(1)
		{	
			if(status==1)
			{
			if(posflag>=(res-1))
			{
//				delay_ms(1);
					reduce_x(3);
			}
			else 
				add_speedx(status);
				go_right(status);// 
				if(front_7_bin && !isBlack)//左1
				{
					posflag++;
					isBlack = 1;
				}
				else if(!front_7_bin && isBlack) 
				{
					isBlack = 0;	
				}
			}
				else //非1档（细）
			{
				add_speedx(status);
				go_right(status);
				if(front_4_bin && !isBlack)//左4
				{
					posflag++;
					isBlack = 1;
				}
				else if(!front_4_bin && isBlack) 
					isBlack = 0;	
			}
		if(posflag>=res)
				break;
		}
	}
		if(dest<addr)
	{
		Speedx =0;
		res=addr-dest;//走
		posflag=0;//
		isBlack=1;
		Speed_left(Speedx);
		delay_ms(100);		
		while(1)
		{	
			if(status==1)
			{
			if(posflag>=res*4/5)
			{
					reduce_x(10);
			}
			else 
				add_speedx(status);
				go_left(status);// 
				if(front_2_bin && !isBlack)//左1
				{
					posflag++;
					isBlack = 1;
				}
				else if(!front_2_bin && isBlack) 
				{
					isBlack = 0;	
				}
			}
				else //非1档（细）
			{
				add_speedx(status);
				go_left(status);
				if(front_4_bin && !isBlack)//左4
				{
					posflag++;
					isBlack = 1;
				}
				else if(!front_4_bin && isBlack) 
					isBlack = 0;	
			}
		if(posflag>=res)
				break;
		}
	}
	lock_disable();
}
//=================================================================================
/***********************************Y轴方向循迹************************************/
//=================================================================================
//通过坐标行驶 dest:目标 addr：所在点
//车 0 1 2 3 4 5 6 7 8
void go_pos_y(char dest,char addr,u8 status)
{
	u8 res=0;
	if(dest>addr)
	{
		Speedy =0;
		res=dest-addr;//走
		posflag=0;//
		isBlack=1;
		Speed_front(Speedy);	
		delay_ms(150);
		while(1)
		{	
			if(status==1)
			{
			if(posflag>=res*4/5)
			{
				reduce_y(4);
			}
			else 
				add_speedy(status);
				go_front(status);// 
				if(left_3_bin && !isBlack)//左1
				{
					posflag++;
					isBlack = 1;
				}
				else if(!left_3_bin && isBlack) 
				{
					isBlack = 0;	
				}
			}
				else //非1档（细）
			{
				add_speedy(status);
				go_front(status);
				if(left_3_bin && !isBlack)//左4
				{
					posflag++;
					isBlack = 1;
				}
				else if(!left_3_bin && isBlack) 
					isBlack = 0;	
			}
		if(posflag>=res)
				break;
		}
	}
		if(dest<addr)
	{
		Speedy =0;
		res=addr-dest;//走
		posflag=0;//
		isBlack=1;
		Speed_back(Speedy);
		delay_ms(100);		
		while(1)
		{	
			if(status==1)
			{
			if(posflag>=res*4/5)
			{
					reduce_y(4);
			}
			else 
				add_speedy(status);
				go_back(status);// 
				if(right_6_bin && !isBlack)//左1
				{
					posflag++;
					isBlack = 1;
				}
				else if(!right_6_bin && isBlack) 
				{
					isBlack = 0;	
				}
			}
				else //非1档（细）
			{
				add_speedy(status);
				go_back(status);
				if(right_6_bin && !isBlack)//左4
				{
					posflag++;
					isBlack = 1;
				}
				else if(!right_6_bin && isBlack) 
					isBlack = 0;	
			}
		if(posflag>=res)
				break;
		}
	}
	lock_disable();
}

//=================================================================================
/***********************************二维码专用循迹*********************************/
//=================================================================================
//通过坐标行驶 dest:目标 addr：所在点
//车 0 1 2 3 4 5 6 7 8
void go_pos_code(char dest,char addr)
{
		u8 res=0;
		res=dest-addr;//走
		posflag=0;//
		isBlack=1;
		Speed_right(14);	
		while(1)
		{	
				if(right_2_bin && !right_7_bin)
				{
					M1_speed(Speedx*2/3);
					M3_speed(Speedx);
				}
				 else if(right_7_bin && !right_2_bin)
				{
					M1_speed(Speedx);
					M3_speed(Speedx*2/3);
				}
				else
				{
					M1_speed(Speedx);
					M3_speed(Speedx);
				}
			if(left_1_bin && !isBlack)//
			{
				posflag++;
				isBlack = 1;
			}
			else if(!left_1_bin && isBlack) 
				isBlack = 0;	
			if(posflag>=res)
				{
						break;
				}
		}
		lock_disable();
}

void reduce_x(u8 speed)
{
	if(Speedx>=speed)
	{
		Speedx--;
		delay_ms(1);
		M2_speed(Speedx);
		M4_speed(Speedx);
	}	
}

void reduce_y(u8 speed)
{
	if(Speedy>=speed)
	{
		Speedy--;
		delay_ms(2);
		M1_speed(Speedy);
		M3_speed(Speedy);
	}	
}

void go_0_1(void)	//从原料区出发去粗加工区
{
	go_pos_y(4,1,0);
	adjust();
	turn_90(1);
	adjust();
//	lock_disable();

	Speed_back(8);
	while(!right_8_bin && !left_8_bin)
	{
		if(back_2_bin && !back_7_bin)
		{
			M2_speed(Speedy*2/3);
			M4_speed(Speedy);
		}
		 else if(back_7_bin && !back_2_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*2/3);
		}
		else if(back_3_bin && !back_6_bin)
		{
			M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		 else if(back_6_bin && !back_3_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(back_4_bin && !back_5_bin)
		{
			M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		 else if(back_5_bin && !back_4_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(back_1_bin && !back_8_bin)
		{
			M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		 else if(back_8_bin && !back_1_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
	}
	delay_ms(20);
	lock_disable();
//		while((front_1_bin || front_2_bin || front_3_bin))
//	{
//		Speed_left(10);
//		while(!front_5_bin);
//		lock_disable();
//	}
	adjust_X();
//	while(front_4_bin == 0)
//	{
//		Speed_right(10);
//		while(!front_4_bin);
//		lock_disable();
//	}
}

void go_0_1_two(void)	//从原料区出发去粗加工区
{
	go_pos_y(4,2,0);
	adjust();
	turn_90(1);
	adjust();
//	lock_disable();

	Speed_back(8);
	while(!right_8_bin && !left_8_bin)
	{
		if(back_2_bin && !back_7_bin)
		{
			M2_speed(Speedy*2/3);
			M4_speed(Speedy);
		}
		 else if(back_7_bin && !back_2_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*2/3);
		}
		else if(back_3_bin && !back_6_bin)
		{
			M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		 else if(back_6_bin && !back_3_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(back_4_bin && !back_5_bin)
		{
			M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		 else if(back_5_bin && !back_4_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(back_1_bin && !back_8_bin)
		{
			M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		 else if(back_8_bin && !back_1_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
	}
	delay_ms(20);
	lock_disable();
	adjust_X();
//		while((front_1_bin || front_2_bin || front_3_bin))
//	{
//		Speed_left(10);
//		while(!front_5_bin);
//		lock_disable();
//	}
//	
//	while(front_4_bin == 0)
//	{
//		Speed_right(10);
//		while(!front_4_bin);
//		lock_disable();
//	}
}

void go_1_2(void)	//从粗加工区出发去半加工区
{
	go_pos_y(4,1,0);
	adjust();
	//
	turn_90(1);
	adjust();
	//
	go_pos_y(2,4,1);
	Speed_back(8);
	while(!right_8_bin && !left_8_bin)
	{
		if(back_2_bin && !back_7_bin)
		{
			M2_speed(Speedy*2/3);
			M4_speed(Speedy);
		}
		 else if(back_7_bin && !back_2_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*2/3);
		}
		else if(back_3_bin && !back_6_bin)
		{
			M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		 else if(back_6_bin && !back_3_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(back_4_bin && !back_5_bin)
		{
			M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		 else if(back_5_bin && !back_4_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(back_1_bin && !back_8_bin)
		{
			M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		 else if(back_8_bin && !back_1_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
	}
	
	lock_disable();
	adjust_X();
//		while((front_1_bin || front_2_bin || front_3_bin))
//	{
//		Speed_left(10);
//		while(!front_5_bin);
//		lock_disable();
//	}
//	
//	while(front_4_bin == 0)
//	{
//		Speed_right(10);
//		while(!front_4_bin);
//		lock_disable();
//	}
	
}

void go_1_2_two(void)	//从粗加工区出发去半加工区
{
	go_pos_y(4,1,0);
	adjust();
	//
	turn_90(1);
	adjust();
	//
	go_pos_y(2,4,1);
	Speed_back(8);
	while(!right_8_bin && !left_8_bin)
	{
		if(back_2_bin && !back_7_bin)
		{
			M2_speed(Speedy*2/3);
			M4_speed(Speedy);
		}
		 else if(back_7_bin && !back_2_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*2/3);
		}
		else if(back_3_bin && !back_6_bin)
		{
			M2_speed(Speedy*3/4);
			M4_speed(Speedy);
		}
		 else if(back_6_bin && !back_3_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*3/4);
		}
		else if(back_4_bin && !back_5_bin)
		{
			M2_speed(Speedy*4/5);
			M4_speed(Speedy);
		}
		 else if(back_5_bin && !back_4_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*4/5);
		}
		else if(back_1_bin && !back_8_bin)
		{
			M2_speed(Speedy*1/2);
			M4_speed(Speedy);
		}
		 else if(back_8_bin && !back_1_bin)
		{
			M2_speed(Speedy);
			M4_speed(Speedy*1/2);
		}
		else
		{
			M2_speed(Speedy);
			M4_speed(Speedy);
		}
	}
	lock_disable();
//		while((front_1_bin || front_2_bin || front_3_bin))
//	{
//		Speed_left(10);
//		while(!front_5_bin);
//		lock_disable();
//	}
//	
//	while(front_4_bin == 0)
//	{
//		Speed_right(10);
//		while(!front_4_bin);
//		lock_disable();
//	}
	adjust_X0();
	
}

void go_2_1(void)	//从半加工区出发去粗加工区
{
	go_pos_x(1,4,1);
//	turn_90(0);
	adjust();
	turn_90(0);
	adjust();
	go_pos_x(6,4,1);
	while((right_2_bin == 0) && (right_7_bin == 0) )
	{
		Speed_right(8);
		if(right_3_bin && !right_6_bin)
		{
			M1_speed(Speedx*3/4);
			M3_speed(Speedx);
		}
		 else if(right_6_bin && !right_3_bin)
		{
			M1_speed(Speedx);
			M3_speed(Speedx*3/4);
		}
		else
		{
			M1_speed(Speedx);
			M3_speed(Speedx);
		}
	}
//	if(QR_dat[m+1] == '2' )
//	{
//		delay_ms(70);
////		break;
//	}
//	if(QR_dat[m+1] == '3')
//	{
//		delay_ms(75);
//	}
	lock_disable();
}

void go_2_0(void)	//从半加工区出发去原料区
{
	go_pos_y(2,1,0);
//	adjust();
	//
	turn_90(0);
	adjust();
	while((front_1_bin || front_2_bin || front_3_bin))
	{
		Speed_left(10);
		while(!front_5_bin);
		lock_disable();
	}
	
	while(front_4_bin == 0)
	{
		Speed_right(10);
		while(!front_4_bin);
		lock_disable();
	}
	go_pos_y(2,4,0);
	
	adjust();
	//
	turn_90(0);
//	adjust();
	while((front_1_bin || front_2_bin || front_3_bin))
	{
		Speed_left(10);
		while(!front_5_bin);
		lock_disable();
	}
	
	while(front_4_bin == 0)
	{
		Speed_right(10);
		while(!front_4_bin);
		lock_disable();
	}
	go_pos_y(2,6,1);
}


/**********************************************************
	
	矫正车身
	
***********************************************************/
void adjust(void)
{
	adjust_X();
	adjust_Y();
}

void adjust_Y()
{

	while((left_1_bin || left_2_bin || left_3_bin || left_6_bin || left_7_bin || left_8_bin )&& (right_1_bin || right_2_bin || right_3_bin || right_6_bin || right_7_bin || right_8_bin))
		{
			M_run(0,1,0,1);
			M2_speed(5);
				if(left_1_bin || left_2_bin || left_3_bin) M2_dir(Front);
				if(left_6_bin || left_7_bin || left_8_bin) M2_dir(Back);
			M4_speed(5);
				if(right_1_bin || right_2_bin || right_3_bin) M4_dir(Front);
				if(right_6_bin || right_7_bin || right_8_bin) M4_dir(Back);
		}
		lock_disable();
		
	while(left_1_bin || left_2_bin || left_3_bin || left_6_bin || left_7_bin || left_8_bin || right_1_bin || right_2_bin || right_3_bin || right_6_bin || right_7_bin || right_8_bin)
	{

		while(left_1_bin || left_2_bin || left_3_bin || left_6_bin || left_7_bin || left_8_bin)
		{
				M_run(0,1,0,0);
				M2_speed(5);
				if(left_1_bin || left_2_bin || left_3_bin) M2_dir(Front);
				if(left_6_bin || left_7_bin || left_8_bin) M2_dir(Back);
		}
			 lock_disable();
			while(right_1_bin || right_2_bin || right_3_bin || right_6_bin || right_7_bin || right_8_bin)
		{
				M_run(0,0,0,1);
				M4_speed(5);
				if(right_1_bin || right_2_bin || right_3_bin) M4_dir(Front);
				if(right_6_bin || right_7_bin || right_8_bin) M4_dir(Back );
		}
			  lock_disable();
 	}
}

void adjust_X()
{
	while((front_1_bin || front_2_bin || front_3_bin || front_6_bin || front_7_bin || front_8_bin) && (back_1_bin || back_2_bin || back_3_bin || back_6_bin || back_7_bin || back_8_bin))
	{
		M_run(1,0,1,0);
		M1_speed(5);
		M3_speed(5);
		if(front_1_bin || front_2_bin || front_3_bin) M1_dir(Left);
		if(front_6_bin || front_7_bin || front_8_bin) M1_dir(Right);
		if(back_1_bin || back_2_bin || back_3_bin) M3_dir(Left);
		if(back_6_bin || back_7_bin || back_8_bin) M3_dir(Right);
	}
	while(front_1_bin || front_2_bin || front_3_bin || front_6_bin || front_7_bin || front_8_bin || back_1_bin || back_2_bin || back_3_bin || back_6_bin || back_7_bin || back_8_bin)
	{
		while(front_1_bin || front_2_bin || front_3_bin || front_6_bin || front_7_bin || front_8_bin)
		{
				M_run(1,0,0,0);
				M1_speed(5);
				if(front_1_bin || front_2_bin || front_3_bin) M1_dir(Left);
				if(front_6_bin || front_7_bin || front_8_bin) M1_dir(Right);
		}
			lock_disable();
			while(back_1_bin || back_2_bin || back_3_bin || back_6_bin || back_7_bin || back_8_bin)
		{
				M_run(0,0,1,0);
				M3_speed(5);
				if(back_1_bin || back_2_bin || back_3_bin) M3_dir(Left);
				if(back_6_bin || back_7_bin || back_8_bin) M3_dir(Right);
		}
			lock_disable();
 }
}

//void adjust00()
//{

//		while(right_3_bin || right_6_bin)
//		{
//				M_run(0,0,0,1);
//				M4_speed(5);
//				if(right_3_bin) M4_dir(Front);
//				if(right_6_bin) M4_dir(Back);
//		}
//			  M_lock();
//	  delay_ms(100);
//	  M_run(0,0,0,0);
//	
//}

void adjust_X0()
{
		while(front_1_bin || front_2_bin || front_3_bin || front_6_bin || front_7_bin || front_8_bin)
		{
				M_run(1,0,0,0);
				M1_speed(5);
				if(front_1_bin || front_2_bin || front_3_bin) M1_dir(Left);
				if(front_6_bin || front_7_bin || front_8_bin) M1_dir(Right);
		}
			lock_disable();
 }



