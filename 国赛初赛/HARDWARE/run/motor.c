#include "sys.h"
#include "motor.h"
#include "delay.h"
#include "LED.h"

#define M1_Step PAout(0)
#define M2_Step PAout(4)
#define M3_Step PBout(4)
#define M4_Step PAout(8)

u8 step[4][2]={1,1,0,1,0,0,1,0};    //电机步伐
u8 i=0;
   u8 Smotor1_speed,         //速度调节
      Smotor2_speed,
	    Smotor3_speed,
	    Smotor4_speed;
	 u8 Smotor1_dir = 1,          //电机方向
			Smotor2_dir = 1,
			Smotor3_dir = 1,
			Smotor4_dir = 1;
   u8 isM1Run = 1,
      isM2Run = 1,
			isM3Run = 1,
			isM4Run = 1;

void M_run(u8 a,u8 b,u8 c,u8 d)   
     {
       TIM_Cmd(TIM2,ENABLE);
		   PBout(3)=!a; PAout(6)=!b; PBout(14)=!c; PBout(15)=!d;
		 }
/**********************************************
		 
		 锁住电机 保持力矩  电流大  不宜长时间
		 
**********************************************/
void M_lock()
{
  TIM_Cmd(TIM2,DISABLE);
	PBout(3)=0; PAout(6)=0; PBout(14)=0; PBout(15)=0;
}
/********************************************

								中断控制电机

********************************************/
void TIM2_IRQHandler(void)   //TIM2中断
{

		static u8 Amotor1=0;
	  static u8 Amotor2=0;
	  static u8 Amotor3=0;
	  static u8 Amotor4=0;
	/*	static u8 Stmotor1=0;             //电机步 0~3
	  static u8 Stmotor2=0;
	  static u8 Stmotor3=0;
	  static u8 Stmotor4=0;*/
	
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)  //检查TIM2更新中断发生与否
		{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update  );  //清除TIMx更新中断标志 
			
			Amotor1++;Amotor2++;
			Amotor3++;Amotor4++;                    //电机驱动速度基础值自加
			
			
			/**********************************************
			
						电机1 控制
			********************************************/			
      if(isM1Run)
			if(Amotor1>=Smotor1_speed)                       //速度选择
			{
				Amotor1=0;
				M1_Step = !M1_Step;
				
			}
			
			
			/**********************************************
			
						电机2 控制
			********************************************/			
      if(isM2Run)
			if(Amotor2>=Smotor2_speed)                       //速度选择
			{
				Amotor2=0;
				M2_Step = !M2_Step;
			}
			
			/**********************************************
			
						电机3 控制
			********************************************/
      if(isM3Run)
			if(Amotor3>=Smotor3_speed)                       //速度选择
			{
				Amotor3=0;
				M3_Step = !M3_Step;
			}

			/**********************************************
			
						电机4 控制
			********************************************/
		  if(isM4Run)
			if(Amotor4>=Smotor4_speed)
			{ 
			Amotor4=0;
					M4_Step = !M4_Step;	
			}
		
	}
}


void TIM5_IRQHandler(void)    //控制使能pwm  
{ 	
  static	u16 i=0;
		if (TIM_GetITStatus(TIM5, TIM_IT_Update) != RESET)  //检查TIM2更新中断发生与否
		{
		TIM_ClearITPendingBit(TIM5, TIM_IT_Update  );  //清除TIMx更新中断标志 
			i++;
			if(i>=20)i=0;
			else if(i<19) {PBout(3)=1; PAout(6)=1; PBout(14)=1; PBout(15)=1;LED0 = 0;}//
			else {PBout(3)=0; PAout(6)=0; PBout(14)=0; PBout(15)=0;LED0 = 1;}
			//LED0 = !LED0;
		}

}

void motor_Init(void)
{
 
 GPIO_InitTypeDef  GPIO_InitStructure;
 	
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO, ENABLE);	 //使能PB,PE端口时钟
	
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_12|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_11;				 //LED0-->PB.5 端口配置
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOA, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5
 GPIO_SetBits(GPIOA,GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_11);						 //PB.5 输出高
	
 GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE); //关闭JTAG PB3可做IO
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_14|GPIO_Pin_15;				 //LED0-->PB.5 端口配置
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOB, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5
 GPIO_SetBits(GPIOB,GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_14|GPIO_Pin_15);						 //PB.5 输出高

}

