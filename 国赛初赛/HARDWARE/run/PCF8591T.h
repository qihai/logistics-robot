#ifndef  _PCF8591T_H_
#define  _PCF8591T_H_
#include "sys.h"

///*********************************************
//		前传感器定义

//*********************************************/
#define front_gl_1     get_ad(0x92,0x43)
#define front_gl_2     get_ad(0x92,0x42)
#define front_gl_3     get_ad(0x92,0x41)
#define front_gl_4     get_ad(0x92,0x40)

#define front_gl_5     get_ad(0x90,0x43)
#define front_gl_6     get_ad(0x90,0x42)
#define front_gl_7     get_ad(0x90,0x41)
#define front_gl_8     get_ad(0x90,0x40)

///*********************************************
//	后传感器定义

//*********************************************/

#define back_gl_1      get_ad(0x94,0x40)
#define back_gl_2      get_ad(0x94,0x41)
#define back_gl_3      get_ad(0x94,0x42)
#define back_gl_4      get_ad(0x94,0x43)

#define back_gl_5      get_ad(0x96,0x40)
#define back_gl_6      get_ad(0x96,0x41)
#define back_gl_7      get_ad(0x96,0x42)
#define back_gl_8      get_ad(0x96,0x43)

///*********************************************
//		左传感器定义

//*********************************************/

#define left_gl_1      get_ad(0x98,0x40)
#define left_gl_2      get_ad(0x98,0x41)
#define left_gl_3      get_ad(0x98,0x42)
#define left_gl_4      get_ad(0x98,0x43)

#define left_gl_5      get_ad(0x9A,0x40)
#define left_gl_6      get_ad(0x9A,0x41)
#define left_gl_7      get_ad(0x9A,0x42)
#define left_gl_8      get_ad(0x9A,0x43)

///*********************************************
//		右传感器定义

//*********************************************/

#define right_gl_1     get_ad(0x9E,0x43)
#define right_gl_2     get_ad(0x9E,0x42)
#define right_gl_3     get_ad(0x9E,0x41)
#define right_gl_4     get_ad(0x9E,0x40)

#define right_gl_5     get_ad(0x9C,0x43)
#define right_gl_6     get_ad(0x9C,0x42)
#define right_gl_7     get_ad(0x9C,0x41)
#define right_gl_8     get_ad(0x9C,0x40)


/*******************************************

		返回传感器二值  1压线 0没压线

********************************************/

#define front_1_bin      get_AD_bin(0x92,0x43)
#define front_2_bin      get_AD_bin(0x92,0x42)
#define front_3_bin      get_AD_bin(0x92,0x41)
#define front_4_bin      get_AD_bin(0x92,0x40)

#define front_5_bin      get_AD_bin(0x90,0x43)
#define front_6_bin      get_AD_bin(0x90,0x42)
#define front_7_bin      get_AD_bin(0x90,0x41)
#define front_8_bin      get_AD_bin(0x90,0x40)

#define back_1_bin       get_AD_bin(0x94,0x40)
#define back_2_bin       get_AD_bin(0x94,0x41)
#define back_3_bin       get_AD_bin(0x94,0x42)
#define back_4_bin       get_AD_bin(0x94,0x43)

#define back_5_bin       get_AD_bin(0x96,0x40)
#define back_6_bin       get_AD_bin(0x96,0x41)
#define back_7_bin       get_AD_bin(0x96,0x42)
#define back_8_bin       get_AD_bin(0x96,0x43)

#define left_1_bin       get_ADr_bin(0x98,0x40)
#define left_2_bin       get_ADr_bin(0x98,0x41)
#define left_3_bin       get_ADr_bin(0x98,0x42)
#define left_4_bin       get_ADr_bin(0x98,0x43)

#define left_5_bin       get_ADr_bin(0x9A,0x40)
#define left_6_bin       get_ADr_bin(0x9A,0x41)
#define left_7_bin       get_ADr_bin(0x9A,0x42)
#define left_8_bin       get_ADr_bin(0x9A,0x43)

#define right_1_bin      get_AD_bin(0x9E,0x43)
#define right_2_bin      get_AD_bin(0x9E,0x42)
#define right_3_bin      get_AD_bin(0x9E,0x41)
#define right_4_bin      get_AD_bin(0x9E,0x40)

#define right_5_bin      get_AD_bin(0x9C,0x43)
#define right_6_bin      get_AD_bin(0x9C,0x42)
#define right_7_bin      get_AD_bin(0x9C,0x41)
#define right_8_bin      get_AD_bin(0x9C,0x40)

/*****************************************
		返回传感器AD值
*****************************************/
int PCF8591T_Read_AD(u8 addr , u8 data);
u8 *front_left_AD(void);      
u8 *front_right_AD(void);
u8 *back_right_AD(void);
u8 *back_left_AD(void);
u8 *left_front_AD(void);
u8 *left_back_AD(void);
u8 *right_front_AD(void);
u8 *right_back_AD(void);


u8 get_ADr_bin(u8 addr,u8 dat);
u8 get_AD_bin(u8 addr,u8 dat);
int get_ad(u8 addr,u8 dat);
#endif

