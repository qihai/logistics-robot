#ifndef __MOTOR_H_
#define __MOTOR_H_
#include "sys.h"


extern	u8	Smotor1_speed;
extern	u8	Smotor2_speed;
extern	u8	Smotor3_speed;
extern	u8	Smotor4_speed;
extern	u8	Smotor1_dir ,          //�������
						Smotor2_dir ,
						Smotor3_dir ,
						Smotor4_dir ;
extern  u8 	isM1Run,
						isM2Run,
						isM3Run,
						isM4Run;

#define Front 1
#define Back  0
#define Left 1
#define Right  0
#define Run   1
#define Stop  0


#define M1_speed(n)   Smotor1_speed = 24-(n)
#define M2_speed(n)   Smotor2_speed = 24-(n)
#define M3_speed(n)   Smotor3_speed = 24-(n)
#define M4_speed(n)   Smotor4_speed = 24-(n)

#define M1_dir(n)     PAout(1) = n
#define M2_dir(n)     PAout(5) = !n
#define M3_dir(n)     PAout(7) = !n
#define M4_dir(n)     PAout(11) = n

void M_run(u8 a,u8 b,u8 c,u8 d);
void motor_Init(void);
void M_lock(void);

#endif
