#include "myiic.h"
#include "PCF8591T.h"
#include "delay.h"
#include "stdio.h"
#include "string.h"

/**************前传感器从左到右*************************/
//u8 U1_addr[8][2] = {{0x92,0x42},{0x92,0x41},{0x92,0x40},{0x92,0x43},{0x90,0x40},{0x90,0x43},{0x90,0x42},{0x90,0x41}};


/********************************************************


		读取对应的ADC数值


********************************************************/

int PCF8591T_Read_AD(u8 addr , u8 data)
{
		  int dat;
			IIC_Start();
			IIC_Send_Byte(addr);             //写入地址
		  IIC_Wait_Ack();
			IIC_Send_Byte(data);             //写入数据
		  IIC_Wait_Ack();
			IIC_Start();
		  IIC_Send_Byte(addr+1);           //写入读命令
		  IIC_Wait_Ack();
			dat =IIC_Read_Byte(0);	      //读取ad
			IIC_Stop();
	    return dat;
}


int get_ad(u8 addr,u8 dat)
{
		int sum=0;
	  int i;
	  for(i=0;i<3;i++)
	{
			sum += PCF8591T_Read_AD(addr,dat);
		  delay_us(80);
	}
	return sum/3;
}


///*****************************************************

//			前传感器AD值

//******************************************************/
/*
u8 *front_left_AD(void)
{
			u8 str[100];
			int A1,A2,A3,A4;
			A1=front_gl_1;
			A2=front_gl_2;
			A3=front_gl_3;
			A4=front_gl_4;
			sprintf((char*)str,"f_l:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}

u8 *front_right_AD(void)
{
      u8 str[100];
			int A1,A2,A3,A4;
			A1=front_gl_5;
			A2=front_gl_6;
			A3=front_gl_7;
			A4=front_gl_8;
			sprintf((char*)str,"f_r:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}
*/
///*********************************************************

//		后传感器AD值

//*********************************************************/
/*
u8 *back_left_AD(void)
{
			u8 str[100];
			int A1,A2,A3,A4;
			A1=back_gl_1;
			A2=back_gl_2;
			A3=back_gl_3;
			A4=back_gl_4;
			sprintf((char*)str,"b_l:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}

u8 *back_right_AD(void)
{
      u8 str[100];
			int A1,A2,A3,A4;
			A1=back_gl_5;
			A2=back_gl_6;
			A3=back_gl_7;
			A4=back_gl_8;
			sprintf((char*)str,"b_r:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}
*/

///*********************************************************

//	左传感器AD值

//*********************************************************/
/*
u8 *left_front_AD(void)
{
			u8 str[100];
			int A1,A2,A3,A4;
			A1=left_gl_1;
			A2=left_gl_2;
			A3=left_gl_3;
			A4=left_gl_4;
			sprintf((char*)str,"l_f:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}

u8 *left_back_AD(void)
{
      u8 str[100];
			int A1,A2,A3,A4;
			A1=left_gl_5;
			A2=left_gl_6;
			A3=left_gl_7;
			A4=left_gl_8;
			sprintf((char*)str,"l_b:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}
*/

///*********************************************************

//	右传感器AD值

//*********************************************************/
/*
u8 *right_front_AD(void)
{
			u8 str[100];
			int A1,A2,A3,A4;
			A1=right_gl_1;
			A2=right_gl_2;
			A3=right_gl_3;
			A4=right_gl_4;
			sprintf((char*)str,"r_f:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}

u8 *right_back_AD(void)
{
      u8 str[100];
			int A1,A2,A3,A4;
			A1=right_gl_5;
			A2=right_gl_6;
			A3=right_gl_7;
			A4=right_gl_8;
			sprintf((char*)str,"r_b:%3d,%3d,%3d,%3d",A1,A2,A3,A4);
			return str;
}
*/
/******************************************************

			压线返回1 没压线返回0

******************************************************/

u8 get_AD_bin(u8 addr,u8 dat)
{
			if(get_ad(addr,dat) >= 170) return 1;	//
			else 	return 0;
}
u8 get_ADr_bin(u8 addr,u8 dat)
{
			if(get_ad(addr,dat) >= 180) return 1;	
			else 	return 0;
}

