#ifndef __RGB_H_
#define __RGB_H_
#include "sys.h"

#define  ON  0x00
#define  OFF 0xA0

void GY_led(u8 x);
int GY33_Read(u8 addr , u8 data);
u8 get_RGB(void); 
u8 read_RGB(void);
#endif

