#ifndef __RGBIIC_H
#define __RGBIIC_H
#include "sys.h"

 
#define RGBSDA_IN()  {GPIOC->CRH&=0XFFFFF0FF;GPIOC->CRH|=(u32)8<<8;}
#define RGBSDA_OUT() {GPIOC->CRH&=0XFFFFF0FF;GPIOC->CRH|=(u32)3<<8;}

//IO操作函数	 
#define RGBIIC_SCL    PCout(11) //SCL
#define RGBIIC_SDA    PCout(10) //SDA	 
#define RGBREAD_SDA   PCin(10)  //输入SDA 

//IIC所有操作函数
void RGBIIC_Init(void);                //初始化IIC的IO口				 
void RGBIIC_Start(void);				//发送IIC开始信号
void RGBIIC_Stop(void);	  			//发送IIC停止信号
void RGBIIC_Send_Byte(u8 txd);			//IIC发送一个字节
u8 RGBIIC_Read_Byte(unsigned char ack);//IIC读取一个字节
u8 RGBIIC_Wait_Ack(void); 				//IIC等待ACK信号
void RGBIIC_Ack(void);					//IIC发送ACK信号
void RGBIIC_NAck(void);				//IIC不发送ACK信号
  
#endif
















