#include "sys.h"


void GY_led(u8 x)
{
		  RGBIIC_Start();
			RGBIIC_Send_Byte(0xB4);             //写入地址
		  RGBIIC_Wait_Ack();
	    RGBIIC_Send_Byte(0x10); 
      RGBIIC_Wait_Ack();	
	    RGBIIC_Send_Byte(x); 
	    RGBIIC_Wait_Ack();
	    RGBIIC_Stop();
}


int GY33_Read(u8 addr , u8 data)
{
		  int dat = 0;
			RGBIIC_Start();
			RGBIIC_Send_Byte(addr);             //写入地址
		  RGBIIC_Wait_Ack();
			RGBIIC_Send_Byte(data);             //写入数据
		  RGBIIC_Wait_Ack();
	    RGBIIC_Start();
		  RGBIIC_Send_Byte(addr+1);           //写入读命令
		  RGBIIC_Wait_Ack();
			dat =RGBIIC_Read_Byte(1);	      //读取ad
			RGBIIC_Stop();
	    return dat;
}

u8 get_RGB()      // RED 0  GREEN 1  BLUE 2
{
	int R,G,B;
	R = GY33_Read(0xB4,0x00);delay_ms(10);  R=R*256+GY33_Read(0xB4,0x01);delay_ms(10);
	G = GY33_Read(0xB4,0x02);delay_ms(10);  G=G*256+GY33_Read(0xB4,0x03);delay_ms(10);
	B = GY33_Read(0xB4,0x04);delay_ms(10);  B=B*256+GY33_Read(0xB4,0x05);delay_ms(10);
 OLED_ShowNum(16,0,R,4,8);OLED_ShowNum(16*3,0,G,4,8);OLED_ShowNum(16*5,0,B,4,8);
	if(R>=G && R>=B) return 1;
	else if(G>=R && G>=B) return 2;
	else return 3;
	
}

//u8 read_RGB()
//{
//	u8 i,RGB=0;
//	int R=0,G=0,B=0;
//	for(i=0;i<50;i++)
//	{
//		 taskENTER_CRITICAL(); 
//			RGB = get_RGB();
//		 taskEXIT_CRITICAL();
//		
//		 if(RGB == 1) R++;
//		 else if(RGB == 2) G++;
//		 else if(RGB == 3) B++;
//		 delay_us(200);
//	}
//	
//	//OLED_ShowNum(16,3,R,3,8);OLED_ShowNum(16*2,3,G,3,8);OLED_ShowNum(16*3,3,B,3,8);
//	
//	if(R>G && R>B) return 1;
//	if(G>R && G>B) return 2;
//	if(B>R && B>G) return 3;
//}
