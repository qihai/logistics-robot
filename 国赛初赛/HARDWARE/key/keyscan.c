#include "sys.h"
#include "delay.h"
#include "keyscan.h"
void Key_init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC, ENABLE);
	
	 
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_12;				 //key1--> PB12 key--->PB13
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 		 //输入高
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOB, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB12 13
	
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;				 //key1--> PB12 key--->PB13
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 		 //输入高
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
 GPIO_Init(GPIOC, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB12 13
 
}

/*****************************************************
key1按下返回1，key2按下返回2
*****************************************************/

u8 keyscan()
{
		static u8 isKeyDonw = 0;   //0按键按下 1按键没有按下
	  static u16 t=0;
		if((!PBin(12) || !PBin(13) ) && isKeyDonw == 0)
		{
			delay_ms(20);
			if((!PBin(12) || !PBin(13) ) && isKeyDonw == 0)
			{
					isKeyDonw = 1;
				  if(PBin(12) == 0 && PBin(13) == 0) return 3;
					else if(PBin(12) == 0) return 1;
				  else if(PBin(13) == 0) return 2;
				  else return 0;
			}	
		}
		
		else if(PBin(12) && PBin(13))					
		{
			isKeyDonw = 0;
			t=0;
		}
		else 
		{
			t++;
			delay_ms(1);
			if(t>=150)
			{
				t=140;
				isKeyDonw = 0;
			}
		}
		return 0;
}

