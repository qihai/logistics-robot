#ifndef _KEYSCAN_H_
#define _KEYSCAN_H_
#include "sys.h"

#define go PCin(12)

void Key_init(void);
u8 keyscan(void);

#endif
