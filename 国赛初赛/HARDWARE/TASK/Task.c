#include "Task.h"

u8 m=0,n=0;

void Task_one(void)
{
//=================================================================================	
	//原料区上层和粗加工区之间的抓取放置
	for(m=0;m<3;m++)
	{
		for(n=0;n<3;n++)
		{
			if(RGB_dat[n] == QR_dat[m])
			{
				if(n == 0)	//j 0 1 2  左 中 右
				{
					Catch_0_1_0();//原料区0上层
					delay_ms(150);
				}
				else if(n == 1)
				{
					Catch_0_1_1();//原料区1上层
					delay_ms(150);
				}
				else if(n == 2)
				{
					Catch_0_1_2();//原料区2上层	
					delay_ms(150);
				}	
//				break;
			}	
		}			
		
		if(QR_dat[m] == '1')
		{
			lay_red();//
		}
		else if(QR_dat[m] == '2')
		{
			lay_green();//
		}
		else if(QR_dat[m] == '3')
		{
			lay_blue();//
		}
	}
		M_run(0,0,0,0);
		go_0_1();	//从原料区出发去粗加工区
	for(m=0;m<3;m++)
	{
		if(QR_dat[m] == '1')
		{
			catch_red();
			delay_ms(150);
			LayDown_1_0();//粗加工区放置
		}
		else if(QR_dat[m] == '2')
		{
			catch_green();
			delay_ms(150);
			LayDown_1_1();//粗加工区放置
		}
		else if(QR_dat[m] == '3')
		{
			catch_blue();
			delay_ms(150);
			LayDown_1_2();//粗加工区放置
		}
//		adjust();

	}

//=================================================================================	
//粗加工区和半加工区上层之间的抓取放置
	//进行抓取运动调节

	for(m=0;m<3;m++)
	{
		if(QR_dat[m] == '1')
		{
			Catch_1_0();//粗加工区抓取
			delay_ms(250);
			lay_red();//
		}
		else if(QR_dat[m] == '2')
		{
			Catch_1_1();//粗加工区抓取
			delay_ms(250);
			lay_green();//
		}
		else if(QR_dat[m] == '3')
		{
			Catch_1_2();//粗加工区抓取
			delay_ms(250);
			lay_blue();//
		}
		delay_ms(100);
	}
	M_run(0,0,0,0);	
	go_1_2();	//从粗加工区出发去半加工区		
	for(m=0;m<3;m++)
	{
		if(QR_dat[m] == '1')
		{
			catch_red();
			delay_ms(150);
			LayDown_2_1_0();//半加工区0上层
		}
		else if(QR_dat[m] == '2')
		{
			catch_green();
			delay_ms(150);
			LayDown_2_1_1();//半加工区0上层
		}
		else if(QR_dat[m] == '3')
		{
			catch_blue();
			delay_ms(150);
			LayDown_2_1_2();//半加工区0上层
		}
//		adjust();

	}		
}

void Task_two(void)
{
//=================================================================================	
	//原料区上层和粗加工区之间的抓取放置
	for(m=4;m<7;m++)
	{
		for(n=0;n<3;n++)
		{
			if(RGB_dat[n] == QR_dat[m])
			{
				if(n == 0)	//j 0 1 2  左 中 右
				{
					Catch_0_2_0();//原料区0上层
					delay_ms(300);
				}
				else if(n == 1)
				{
					Catch_0_2_1();//原料区1上层
					delay_ms(300);
				}
				else if(n == 2)
				{
					Catch_0_2_2();//原料区2上层	
					delay_ms(300);
				}	
				break;
			}	
		}			
		
		if(QR_dat[m] == '1')
		{
			
			lay_red();//
		}
		else if(QR_dat[m] == '2')
		{
			lay_green();//
		}
		else if(QR_dat[m] == '3')
		{
			lay_blue();//
		}
	}
	M_run(0,0,0,0);
	go_0_1_two();
	
	for(m=4;m<7;m++)
	{
		if(QR_dat[m] == '1')
		{
			catch_red();//粗加工区抓取
			delay_ms(250);
			LayDown_1_0();//
		}
		else if(QR_dat[m] == '2')
		{
			catch_green();//粗加工区抓取
			delay_ms(250);
			LayDown_1_1();//
		}
		else if(QR_dat[m] == '3')
		{
			catch_blue();//粗加工区抓取
			delay_ms(250);
			LayDown_1_2();//
		}
//		adjust();

	}
	
	for(m=4;m<7;m++)
	{
		if(QR_dat[m] == '1')
		{
			Catch_1_0();//粗加工区抓取
			delay_ms(250);
			lay_red();//
		}
		else if(QR_dat[m] == '2')
		{
			Catch_1_1();//粗加工区抓取
			delay_ms(250);
			lay_green();//
		}
		else if(QR_dat[m] == '3')
		{
			Catch_1_2();//粗加工区抓取
			delay_ms(300);
			lay_blue();//
		}
//		adjust();
	}
	M_run(0,0,0,0);
	go_1_2_two();
//=================================================================================	
//粗加工区和半加工区上层之间的抓取放置
	//进行抓取运动调节
	for(m=4;m<7;m++)
	{
		delay_ms(100);
		if(QR_dat[m] == '1')
		{
			catch_red();
			delay_ms(250);
			LayDown_2_2_0();//半加工
		}
		else if(QR_dat[m] == '2')
		{
			catch_green();
			delay_ms(250);
			LayDown_2_2_1();//半加工区0上层
		}
		else if(QR_dat[m] == '3')
		{
			catch_blue();
			delay_ms(250);
			LayDown_2_2_2();//半加工区0上层
		}
//		adjust();
	}		
}

